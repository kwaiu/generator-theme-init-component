# README v1.0.9

Yeoman generator for creating WP self contained components 

## Self contained component 

A component is a self-contained building-block for containing it's own fields, PHP template, PHP functions, scripts, and styles.

ExampleComponent/\

  ├── ExampleComponentFunctions.php\
  ├── ExampleComponent.php\
  ├── ExampleComponent.js\
  ├── ExampleComponent.scss

## Installation

`$ npm install -g generator-theme-init-component`

`$ yo theme-init-component`\
    `Initializing...`\
    `? Enter a name for the new component (i.e.: myNewComponent):  myNewComponent`

## Created using this Yeoman guide 

https://medium.com/@vallejos/yeoman-guide-adea4d6ea1e3

## NPM Source 
https://www.npmjs.com/package/generator-theme-init-component

## Bitbucket Repository
https://bitbucket.org/kwaiu/generator-theme-init-component/src/master/

